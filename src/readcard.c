/**********************************************************************
 *                                                                    *
 * File             : readcard.c                                      *
 * Version          : 1.0                                             *
 * Author           : HP Beck                                         *
 * Date             : 10-02-99                                        *
 *                                                                    *
 * History:                                                           *
 * --------                                                           *
 *                                                                    *
 ************ Copyright Fifty Acres and a John Deere 1997   ***********
 */

#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "ROSGetInput/get_input.h"

#define CARD_PARAM 500
#define LINE_SIZE  132
#define END_TAG    "END"

static FILE* fd = NULL;

static char card[CARD_PARAM][LINE_SIZE] ;
static char card_initialized[LINE_SIZE] = "__No Tag__" ;

static enum {NO_TAG,TAG_OK} tag_;

/* ----------------------------------------------------------------- */

static void ReadCard_Init(char *filename, char *tag) 
{

  int  empty,i,
       nr         = 0,
       write_card = 0 ;

  char line[LINE_SIZE];

  tag_ = NO_TAG ;

  if (!fd) {
    /* open card */
    if( (fd = fopen(filename, "r")) == NULL) {
      perror("Error opening card");
      return ;
    }
  } else {
    /* re-open card */
    if( (fd = freopen(filename, "r",fd)) == NULL) {
      perror("Error re-reading card");
      return ;
    }
  }

  /* process card */

  while( fgets(line,LINE_SIZE,fd) != NULL ) {

    if (line[0] == '#') continue; /* skip comment lines */
    if (line[strlen(line)-1] == '\n') line[strlen(line)-1]='\0';  /* chomp */
    empty = 1; i=0;               /* skip empty lines */
    while (empty && line[i] != '\0') {
      if (!isspace((int)line[i])) empty = 0;
      i++;
    }
    if (empty == 1) continue;

    i=0;                          /* chop inline comments */
    while (line[i] != '#' && line[i] != '!' && line[i] != '\0') {
      i++ ;
    }
    line[i] = '\0' ;  /* overwrite '#' or '!' or '\0' with '\0' */

    i=strlen(line)-1 ;              /* chop trailing whitespace */
    while (isspace((int)line[i])) {
      i--;
    }
    i++;
    line[i] = '\0' ;

    if (!i) continue;  /* skip empty lines which do have some comments */
    
    /* check for tags and fill card */
    if (write_card) {            
      if (strcmp(END_TAG,line)==0) { /* We are done */
	strcpy(card_initialized,tag) ;

	if(fclose(fd)) {
	  perror("Error closing card");
	  return ;
	}

	fd = NULL;
	return ; 

      }
      strcpy(card[nr],line) ;
      nr++;
    } else {
      if (strcmp(tag,line)==0) write_card = 1 ; /* We found a tag */
      tag_ = TAG_OK ;
    }

  } /* fgets */

  /* We should'nt get here */

  if (write_card) {
    printf("readcard warning: Found Tag %s but no endtag reached!\n",tag) ;
    return ;
  } else {
    printf("readcard error: Tag %s not found!\n",tag) ;
    return ;
  }
}

/* ----------------------------------------------------------------- */

int ReadCard_int(char* filename, char* tag, int nr, int* i)
{  

  if (strcmp(card_initialized,tag)!=0) ReadCard_Init(filename, tag) ;

  if (tag_ == TAG_OK) {
    *i = (int) strtol(card[nr-1], NULL,0) ;
    return 0 ;
  } else {
    return -1;
  }

}

/* ----------------------------------------------------------------- */

int ReadCard_uint(char* filename, char* tag, int nr, unsigned int* u) 
{

  if (strcmp(card_initialized,tag)!=0) ReadCard_Init(filename, tag) ;
  
  if (tag_ == TAG_OK) {
    *u = strtoul(card[nr-1], NULL,0) ;
    return 0 ;
  } else {
    return -1;
  }

}

/* ----------------------------------------------------------------- */

int ReadCard_str(char* filename, char* tag, int nr, char* c) 
{
  
  if (strcmp(card_initialized,tag)!=0) ReadCard_Init(filename, tag) ;

  if (tag_ == TAG_OK) {
    strcpy(c, card[nr-1]) ;
    return 0 ;
  } else {
    return -1;
  }

}

/* ----------------------------------------------------------------- */
