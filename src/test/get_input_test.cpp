#include <stdio.h>             
#include <sys/types.h>

#include "ROSGetInput/get_input.h"

int main(void)
{
  unsigned int u1, u2;
  unsigned long long ui64, ttt;

  printf("sizeof(unsigned long long) is %lu\n", sizeof(unsigned long long));  
    
  ttt = 0x8811772266335544ll; 
  printf("ttt = 0x%016llx\n", ttt);
  
  printf("Enter unsigned long long     ");
  ui64 = gethexdll(0x1122334455667788ll);
  printf("u_int64_t = 0x%016llx\n", ui64);

  u1 = ui64 & 0xffffffff;
  u2 = ui64 >> 32;
  printf("u1 = 0x%08x\n", u1);
  printf("u2 = 0x%08x\n", u2);

  printf("Enter unsigned long long     ");
  ui64 = gethexdll(ttt);
  printf("u_int64_t = 0x%016llx\n", ui64);

  u1 = ui64 & 0xffffffff;
  u2 = ui64 >> 32;
  printf("u1 = 0x%08x\n", u1);
  printf("u2 = 0x%08x\n", u2);

  return(0);
}
